import { useState, useEffect } from "react";

const Flag = ({ data }) => {
  const [currentText, setCurrentText] = useState("");
  
  useEffect(() => {
    if (currentText.length < data.length) {
      const intervalId = setInterval(() => {
        setCurrentText((prevText) => prevText + data[prevText.length]);
      }, 500);
      
      return () => clearInterval(intervalId);
    }
  }, [currentText, data]);
  
  return (
    <ul style={{ whiteSpace: "pre" }}>
      {currentText.split("").map((char, index) => (
              <li key={index}>{char}</li>
            ))}
    </ul>
  
  
  // const [currentText, setCurrentText] = useState("");
  // const [currentIndex, setCurrentIndex] = useState(0);
  //
  // useEffect(() => {
  //   if (currentIndex < data.length) {
  //     const timeout = setTimeout(() => {
  //       setCurrentText((prevText) => prevText + data[currentIndex]);
  //       setCurrentIndex((prevIndex) => prevIndex + 1);
  //     }, 500);
  //
  //     return () => clearTimeout(timeout);
  //   }
  // }, [currentIndex, data]);
  //
  // return (
  //   <ul>
  //     {currentText.split("").map((char, index) => (
  //       <li key={index}>{char}</li>
  //     ))}
  //   </ul>
  );
};

export default Flag;
