/*
This is a script for step 2
<script>
  function findHiddenUrl() {
    const characters = [];
    const refs = [
        ...document.querySelectorAll(`ul[data-tag*="75"] > li[data-id^="98"] > div[data-class$="35"] > span.value`),
    ];
    refs.forEach(span => {
      const character = span.getAttribute('value');
      characters.push(character);
    });
    
    const url = characters.join('');
    return url;
  }

  const hiddenUrl = findHiddenUrl();
</script>
*/
import { useState, useEffect } from "react";
import Flag from "./Flag";
import "./App.css";

const App = () => {
  const [data, setData] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  
  useEffect(() => {
    const fetchData = async() => {
      try {
        const response = await fetch(
          "https://wgg522pwivhvi5gqsn675gth3q0otdja.lambda-url.us-east-1.on.aws/616e63"
        );
        const html = await response.text();
        setData(html);
      } catch (error) {
        console.error("Error:", error);
      } finally {
        setIsLoading(false);
      }
    }
    fetchData()
      .catch(console.error);
  }, [])
  
  return (
    <div className="App">
      <h1>Hello CodeSandbox</h1>
      {isLoading && <h2>Loading ....</h2>}
      {data && <Flag data={data} />}
    </div>
  );
};

export default App;
